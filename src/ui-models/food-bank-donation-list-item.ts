import { Donation } from '../models/donation';
import { User } from '../models/user';

export interface FoodBankDonationListItem {
  donor_name: string;
  location: string;
  weight: number;
  tags: string;
  receiver_name: string;
  /* for api */
  donation: Donation;
  donor: User;
  receiver: User;
}

export namespace FoodBankDonationListItem {
  export const samples: FoodBankDonationListItem[] = [];
  samples.push({
    donor_name: User.farm.name,
    location: 'Magic Farmland',
    weight: 15,
    tags: 'Fresh Food',
    receiver_name: User.mama.name,
    donation: undefined,
    donor: undefined,
    receiver: undefined,
  });
  samples.push({
    donor_name: User.shop.name,
    location: 'Candy House',
    weight: 2,
    tags: 'Preserved Food',
    receiver_name: User.kid.name,
    donation: undefined,
    donor: undefined,
    receiver: undefined,
  });
}
