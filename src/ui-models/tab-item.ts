export interface TabItem {
  root: string;
  title: string;
  icon: string;
}
