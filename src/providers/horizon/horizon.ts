import { Injectable } from '@angular/core';
import _Horizon_ = require('@horizon/client');
import { Horizon, HorizonConstructor } from 'typestub-horizon-client';
import { User } from '../../models/user';
import { config } from '../../utils/config';

export let _Horizon: HorizonConstructor = _Horizon_;

/*
  Generated class for the HorizonProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HorizonProvider {
  hz: Promise<Horizon>;
  user?: User;

  constructor() {
    console.log('Hello HorizonProvider Provider');
    this.init().then(async hz => {
      hz.currentUser()
        .fetch()
        .subscribe(user => {
          this.user = user as any;
          console.log('current userApi:', user);
        });
      if (config.dev) {
        const Users = hz<User>(User.tableName);
        let users = await Users.fetch().toPromise();
        if (users.length >= 5) {
          return;
        }
        users = User.samples;
        const xs = await Users.store(users).toPromise();
        console.log('init users:', xs);
      }
    });
  }

  init() {
    return (this.hz = new Promise<Horizon>((resolve, reject) => {
      const hz = new _Horizon({
        host: '127.0.0.1:8181',
        authType: 'anonymous',
      });
      console.log('connecting to horizon');
      hz.connect();
      hz.onReady(() => {
        console.log('horizon ready');
        resolve(hz);
      });
      hz.onSocketError(e => {
        console.error('horizon socket error:', e);
        reject(e);
      });
    }));
  }
}
