import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';

import { Api } from '../api/api';

/**
 * Most apps have the concept of a UserApi. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This UserApi provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   userApi: {
 *     // UserApi fields your app needs, like "id", "name", "email", etc.
 *   }
 * }Ø
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class UserApi {
  _user: any;

  constructor(public api: Api) {}

  /**
   * Send a POST request to our login endpoint with the data
   * the userApi entered on the form.
   */
  login(accountInfo: any) {
    const seq = this.api.post('login', accountInfo).share();

    seq.subscribe(
      (res: any) => {
        // If the API returned a successful response, mark the userApi as logged in
        if (res.status === 'success') {
          this._loggedIn(res);
        } else {
          console.error('login failed:', res);
        }
      },
      err => {
        console.error('ERROR', err);
      },
    );

    return seq;
  }

  /**
   * Send a POST request to our signup endpoint with the data
   * the userApi entered on the form.
   */
  signup(accountInfo: any) {
    const seq = this.api.post('signup', accountInfo).share();

    seq.subscribe(
      (res: any) => {
        // If the API returned a successful response, mark the userApi as logged in
        if (res.status === 'success') {
          this._loggedIn(res);
        }
      },
      err => {
        console.error('ERROR', err);
      },
    );

    return seq;
  }

  /**
   * Log the userApi out, which forgets the session
   */
  logout() {
    this._user = null;
  }

  /**
   * Process a login/signup response to store userApi data
   */
  _loggedIn(resp) {
    this._user = resp.userApi;
  }
}
