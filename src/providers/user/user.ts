import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { sha256 } from 'typestub-sha256';
import { User } from '../../models/user';
import { HorizonProvider } from '../horizon/horizon';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
  constructor(public horizonService: HorizonProvider) {
    console.log('Hello UserProvider Provider');
  }

  async signup(
    id: string,
    name: string,
    email: string,
    password: string,
    role: string,
  ) {
    const user: User = {
      id,
      groups: undefined,
      email,
      password_hash: sha256(password),
      name,
      donor: role === 'donor',
      receiver: role === 'receiver',
      food_bank: role === 'food_bank',
    };
    const hz = await this.horizonService.hz;
    return hz<User>(User.tableName)
      .store(user)
      .toPromise();
  }

  async login(email: string, password: string) {
    const search = {} as User;
    search.email = email;
    search.password_hash = sha256(password);
    const hz = await this.horizonService.hz;
    return hz<User>(User.tableName)
      .find(search)
      .fetch()
      .defaultIfEmpty()
      .toPromise();
  }
}
