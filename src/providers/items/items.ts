import { Injectable } from '@angular/core';

import { Item } from '../../models/item';
import { Api } from '../api/api';

@Injectable()
export class Items {
  constructor(public api: Api) {}

  query(params?: any) {
    return this.api.get('/items', params);
  }

  add(item: Item) {
    console.log('add', item);
  }

  delete(item: Item) {
    console.log('delete', item);
  }
}
