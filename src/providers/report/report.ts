import { Injectable } from '@angular/core';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import { from } from 'rxjs/observable/from';
import { ReportRequest } from '../../models/report-request';
import { HorizonProvider } from '../horizon/horizon';

@Injectable()
export class ReportProvider {
  constructor(public horizonService: HorizonProvider) {
    console.log('Hello ReportProvider Provider');
  }

  watchPendingRequest(): Observable<ReportRequest[]> {
    const search = {} as ReportRequest;
    search.completed = false;
    return from(this.horizonService.hz).mergeMap(hz =>
      hz<ReportRequest>(ReportRequest.tableName)
        .findAll(search)
        .watch(),
    );
  }

  completeRequest(req: ReportRequest) {
    req.completed = true;
    return from(this.horizonService.hz)
      .mergeMap(hz => hz(ReportRequest.tableName).store(req))
      .toPromise();
  }
}
