import { Injectable } from '@angular/core';
import 'rxjs/add/operator/mergeMap';
import { Observable } from 'rxjs/Observable';
import { from } from 'rxjs/observable/from';
import { of } from 'rxjs/observable/of';
import { Donation } from '../../models/donation';
import { DonationItem } from '../../models/donation-item';
import { FoodBankDonationListItem } from '../../ui-models/food-bank-donation-list-item';
import { HorizonProvider } from '../horizon/horizon';

/*
  Generated class for the DonationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DonationProvider {
  constructor(public horizonService: HorizonProvider) {
    console.log('Hello DonationProvider Provider');
  }

  async submitDonation(items: DonationItem[], donor_id: string) {
    const donation: Donation = {
      id: undefined,
      create_time: Date.now(),
      items,
      donor_id,
      receiver_id: null,
      receiving_helper: null,
    };
    // delete donation.id;
    const hz = await this.horizonService.hz;
    return hz<Donation>(Donation.tableName)
      .store(donation)
      .toPromise();
  }

  watchLastDonation(donor_id: string): Observable<Donation> {
    const search = {} as Donation;
    search.donor_id = donor_id;
    return from(this.horizonService.hz).mergeMap(hz =>
      hz<Donation>(Donation.tableName)
        .findAll(search)
        .order('create_time', 'descending')
        .limit(1)
        .watch()
        .flatMap(xs => xs),
    );
  }

  watchFoodBankDonationList(): Observable<FoodBankDonationListItem[]> {
    if ('dev') {
      const xs: FoodBankDonationListItem[] = FoodBankDonationListItem.samples;
      return of(xs);
    }
    const search = {} as Donation;
    search.receiver_id = null;
    return from(this.horizonService.hz).mergeMap(
      hz =>
        hz<Donation>(Donation.tableName)
          .findAll(search)
          .watch() as any,
    );
  }
}
