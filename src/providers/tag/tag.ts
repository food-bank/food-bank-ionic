import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Tag } from '../../models/tag';
import { config } from '../../utils/config';
import { HorizonProvider } from '../horizon/horizon';

/*
  Generated class for the TagProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TagProvider {
  constructor(public horizonService: HorizonProvider) {
    console.log('Hello TagProvider Provider');
  }

  async getTagList(): Promise<Tag[]> {
    if (config.dev) {
      return [
        { id: '1', en: 'Canned Food', zh_tw: 'asd' },
        { id: '2', en: 'Fruit', zh_tw: 'asd' },
        { id: '3', en: 'Drinks', zh_tw: 'asd' },
        { id: '4', en: 'Furniture', zh_tw: 'asd' },
        { id: '5', en: 'Metal', zh_tw: 'asd' },
      ];
    }
    const hz = await this.horizonService.hz;
    return hz<Tag>(Tag.tableName)
      .fetch()
      .toPromise();
  }
}
