import { Injectable } from '@angular/core';
import { Nav, NavOptions } from 'ionic-angular';
import { TransitionDoneFn } from 'ionic-angular/navigation/nav-util';
import { isDetailPage } from './master-detail';

/*
  Generated class for the NavProxyProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NavProxyProvider {
  masterNav: Nav;
  detailNav: Nav;
  isOn = true;

  constructor() {
    console.log('Hello NavProxyProvider Provider');
  }

  pushDetail(
    pageOrViewCtrl: any,
    params?: any,
    opts?: NavOptions,
    done?: TransitionDoneFn,
  ): Promise<any> {
    if (this.isOn) {
      return this.detailNav.setRoot(pageOrViewCtrl, params, opts, done);
    } else {
      return this.masterNav.push(pageOrViewCtrl, params, opts, done);
    }
  }

  pushMaster(
    pageOrViewCtrl: any,
    params?: any,
    opts?: NavOptions,
    done?: TransitionDoneFn,
  ): Promise<any> {
    return this.masterNav.push(pageOrViewCtrl, params, opts, done);
  }

  onSplitPaneChanged(isOn: boolean) {
    this.isOn = isOn;
    if (this.masterNav && this.detailNav) {
      if (isOn) {
        return this.activateSplitView();
      } else {
        return this.deactiveSplitView();
      }
    }
  }

  activateSplitView() {
    const currentView = this.masterNav.getActive();
    if (isDetailPage(currentView)) {
      return Promise.all([
        this.masterNav.pop(),
        this.detailNav.setRoot(currentView.component, currentView.data),
      ]);
    }
  }

  deactiveSplitView() {
    const detailView = this.detailNav.getActive();
    this.detailNav.setRoot('PlaceholderPage');
    if (isDetailPage(detailView)) {
      const idx = this.masterNav.getViews().length;
      return this.masterNav.insert(idx, detailView.component, detailView.data);
    }
  }
}
