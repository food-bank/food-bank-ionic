import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoodBankReportRequestListPage } from './food-bank-report-request-list';

@NgModule({
  declarations: [FoodBankReportRequestListPage],
  imports: [IonicPageModule.forChild(FoodBankReportRequestListPage)],
})
export class FoodBankReportRequestListPageModule {}
