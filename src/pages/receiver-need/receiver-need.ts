import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Tag } from '../../models/tag';
import { TagProvider } from '../../providers/tag/tag';

/**
 * Generated class for the ReceiverNeedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-receiver-need',
  templateUrl: 'receiver-need.html',
})
export class ReceiverNeedPage {
  tags: Tag[] = [];
  selectedTags: string[] = [];

  constructor(
    public navCtrl: NavController,
    public tagService: TagProvider,
    public navParams: NavParams,
  ) {
    this.tagService.getTagList().then(xs => {
      this.tags = xs;
      this.selectedTags = xs
        .filter(
          x =>
            x.en.toLowerCase().indexOf('fruit') !== -1 ||
            x.en.toLowerCase().indexOf('drink') !== -1,
        )
        .map(x => x.id);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReceiverNeedPage');
  }
}
