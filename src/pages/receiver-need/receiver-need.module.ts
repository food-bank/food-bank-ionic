import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceiverNeedPage } from './receiver-need';

@NgModule({
  declarations: [ReceiverNeedPage],
  imports: [
    IonicPageModule.forChild(ReceiverNeedPage),
    TranslateModule.forChild(),
  ],
})
export class ReceiverNeedPageModule {}
