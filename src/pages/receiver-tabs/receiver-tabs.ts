import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController } from 'ionic-angular';
import { TabItem } from '../../ui-models/tab-item';

@IonicPage()
@Component({
  selector: 'page-receiver-tabs',
  templateUrl: 'receiver-tabs.html',
})
export class ReceiverTabsPage {
  tabs: TabItem[];

  constructor(
    public navCtrl: NavController,
    public translateService: TranslateService,
  ) {
    this.tabs = [
      {
        root: 'ReceiverNeedPage',
        title: 'TAB_NEED_TITLE',
        icon: 'pricetags',
      },
      {
        root: 'ReceiverRequestPage',
        title: 'TAB_REQUEST_TITLE',
        icon: 'bookmarks',
      },
      {
        root: 'ReceiverSettingPage',
        title: 'TAB_SETTING_TITLE',
        icon: 'cog',
      },
    ];
    const ts = this.tabs.map(x => x.title);
    translateService.get(ts).subscribe(values => {
      this.tabs.forEach(tab => (tab.title = values[tab.title]));
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReceiverTabsPage');
  }
}
