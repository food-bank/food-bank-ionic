import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceiverTabsPage } from './receiver-tabs';

@NgModule({
  declarations: [ReceiverTabsPage],
  imports: [IonicPageModule.forChild(ReceiverTabsPage)],
})
export class ReceiverTabsPageModule {}
