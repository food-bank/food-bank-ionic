import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { showToast } from 'ioniclib/utils/toast';
import { MainPage } from '../';
import { UserApi } from '../../providers';
import { UserProvider } from '../../providers/user/user';
import { config } from '../../utils/config';

interface Account {
  email: string;
  password: string;
}

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: Account = {
    email: '',
    password: '',
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(
    public navCtrl: NavController,
    public userApi: UserApi,
    public userProvider: UserProvider,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
  ) {
    this.translateService.get('LOGIN_ERROR').subscribe(value => {
      this.loginErrorString = value;
    });
    try {
      const s = localStorage.getItem('account');
      const o = JSON.parse(s);
      if (o) {
        this.account = o;
      }
    } catch (e) {}
  }

  ngOnInit(): void {
    if (config.auto) {
    }
  }

  // Attempt to login in through our UserApi service
  doLogin() {
    localStorage.setItem('account', JSON.stringify(this.account));
    this.userProvider
      .login(this.account.email, this.account.password)
      .then(user => {
        if (!user) {
          return showToast(this.toastCtrl, { message: 'Wrong Credential' });
        }
        if (user.donor) {
          return this.navCtrl.push('DonorTabsPage');
        }
        if (user.receiver) {
          return this.navCtrl.push('ReceiverTabsPage');
        }
        if (user.food_bank) {
          return this.navCtrl.push('FoodBankMenuPage');
        }
        showToast(this.toastCtrl, { message: 'invalid user' });
      })
      .catch(e => {
        console.error('failed to login:', e);
        showToast(this.toastCtrl, { message: 'Failed to login' });
      });
    if ('dev') {
      return;
    }
    this.userApi.login(this.account).subscribe(
      resp => {
        this.navCtrl.push(MainPage);
      },
      err => {
        this.navCtrl.push(MainPage);
        // Unable to log in
        const toast = this.toastCtrl.create({
          message: this.loginErrorString,
          duration: 3000,
          position: 'top',
        });
        toast.present();
      },
    );
  }
}
