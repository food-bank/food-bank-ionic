// The page the userApi lands on after opening the app and without a session

export let FirstRunPage = 'TutorialPage';
FirstRunPage = 'WelcomePage';
// FirstRunPage = 'DevPage';

// The main page the userApi will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = 'TabsPage';

// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = 'ListMasterPage';
export const Tab2Root = 'SearchPage';
export const Tab3Root = 'SettingsPage';
