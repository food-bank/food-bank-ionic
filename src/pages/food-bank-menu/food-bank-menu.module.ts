import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { FoodBankMenuPage } from './food-bank-menu';

@NgModule({
  declarations: [FoodBankMenuPage],
  imports: [
    IonicPageModule.forChild(FoodBankMenuPage),
    TranslateModule.forChild(),
  ],
})
export class FoodBankMenuPageModule {}
