import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  IonicPage,
  Nav,
  NavController,
  NavParams,
  Platform,
} from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { ReportRequest } from '../../models/report-request';
import { NavProxyProvider } from '../../providers/nav-proxy/nav-proxy';
import { ReportProvider } from '../../providers/report/report';

/**
 * Generated class for the FoodBankMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-food-bank-menu',
  templateUrl: 'food-bank-menu.html',
})
export class FoodBankMenuPage implements OnInit, OnDestroy {
  @ViewChild('masterNav')
  masterNav: Nav;
  @ViewChild('detailNav')
  detailNav: Nav;

  masterPage;
  detailPage;

  sub: Subscription;
  reports: ReportRequest[] = [];

  constructor(
    platform: Platform,
    public navProxy: NavProxyProvider,
    public navCtrl: NavController,
    public reportService: ReportProvider,
    public navParams: NavParams,
  ) {
    console.log({ navProxy });
    platform.ready().then(() => {
      navProxy.masterNav = this.masterNav;
      navProxy.detailNav = this.detailNav;

      this.masterNav.setRoot('FoodBankMenuListPage', {
        detailNavCtrl: this.detailNav,
      });
      this.detailNav.setRoot('PlaceholderPage');
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoodBankMenuPage');
  }

  ngOnInit(): void {
    this.sub = this.reportService.watchPendingRequest().subscribe(reports => {
      this.reports = reports;
    });
  }

  ngOnDestroy(): void {
    this.sub && this.sub.unsubscribe();
  }
}
