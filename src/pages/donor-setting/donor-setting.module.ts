import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DonorSettingPage } from './donor-setting';

@NgModule({
  declarations: [DonorSettingPage],
  imports: [IonicPageModule.forChild(DonorSettingPage)],
})
export class DonorSettingPageModule {}
