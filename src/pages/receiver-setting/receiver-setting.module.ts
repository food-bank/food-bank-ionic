import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceiverSettingPage } from './receiver-setting';

@NgModule({
  declarations: [ReceiverSettingPage],
  imports: [IonicPageModule.forChild(ReceiverSettingPage)],
})
export class ReceiverSettingPageModule {}
