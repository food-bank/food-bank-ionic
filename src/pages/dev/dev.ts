import { Component, OnDestroy, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { User } from '../../models/user';
import { HorizonProvider } from '../../providers/horizon/horizon';

/**
 * Generated class for the DevPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dev',
  templateUrl: 'dev.html',
})
export class DevPage implements OnInit, OnDestroy {
  user?: User;
  sub: Subscription;

  constructor(
    public navCtrl: NavController,
    public horizonService: HorizonProvider,
    public navParams: NavParams,
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad DevPage');
  }

  ngOnInit(): void {
    this.horizonService.init().then(hz => {
      this.sub = hz
        .currentUser()
        .fetch()
        .subscribe(user => {
          this.user = user as any;
        });
    });
  }

  ngOnDestroy(): void {
    this.sub && this.sub.unsubscribe();
  }
}
