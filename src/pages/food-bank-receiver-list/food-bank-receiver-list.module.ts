import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoodBankReceiverListPage } from './food-bank-receiver-list';

@NgModule({
  declarations: [FoodBankReceiverListPage],
  imports: [IonicPageModule.forChild(FoodBankReceiverListPage)],
})
export class FoodBankReceiverListPageModule {}
