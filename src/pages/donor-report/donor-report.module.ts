import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DonorReportPage } from './donor-report';

@NgModule({
  declarations: [DonorReportPage],
  imports: [IonicPageModule.forChild(DonorReportPage)],
})
export class DonorReportPageModule {}
