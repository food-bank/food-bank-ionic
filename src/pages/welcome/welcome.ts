import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { config } from '../../utils/config';

/**
 * The Welcome Page is a splash page that quickly describes the app,
 * and then directs the userApi to create an account or log in.
 * If you'd like to immediately put the userApi onto a login/signup page,
 * we recommend not using the Welcome page.
 */
@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage implements OnInit {
  constructor(public navCtrl: NavController) {}

  ngOnInit(): void {
    if (config.auto) {
      this.login();
    }
  }

  login() {
    this.navCtrl.push('LoginPage');
  }

  signup() {
    this.navCtrl.push('SignupPage');
  }
}
