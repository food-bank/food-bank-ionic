import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { DonorDonationComponent } from '../../components/donor-donation/donor-donation';
import { DonorReceiverComponent } from '../../components/donor-receiver/donor-receiver';
import { DonorDonatePage } from './donor-donate';

@NgModule({
  declarations: [
    DonorDonatePage,
    DonorDonationComponent,
    DonorReceiverComponent,
  ],
  imports: [
    IonicPageModule.forChild(DonorDonatePage),
    TranslateModule.forChild(),
  ],
})
export class DonorDonatePageModule {}
