import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

enum tabs {
  donation,
  receiver,
}

@IonicPage()
@Component({
  selector: 'page-donor-donate',
  templateUrl: 'donor-donate.html',
})
export class DonorDonatePage {
  tabs = tabs;
  tab: tabs = tabs.donation;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad DonorDonatePage');
  }
}
