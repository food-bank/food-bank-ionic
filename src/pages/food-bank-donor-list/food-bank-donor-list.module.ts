import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoodBankDonorListPage } from './food-bank-donor-list';

@NgModule({
  declarations: [FoodBankDonorListPage],
  imports: [IonicPageModule.forChild(FoodBankDonorListPage)],
})
export class FoodBankDonorListPageModule {}
