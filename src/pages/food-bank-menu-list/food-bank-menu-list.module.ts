import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { FoodBankMenuListPage } from './food-bank-menu-list';

@NgModule({
  declarations: [FoodBankMenuListPage],
  imports: [
    IonicPageModule.forChild(FoodBankMenuListPage),
    TranslateModule.forChild(),
  ],
})
export class FoodBankMenuListPageModule {}
