import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { masterPages } from '../../providers/nav-proxy/master-detail';
import { NavProxyProvider } from '../../providers/nav-proxy/nav-proxy';
import { TabItem } from '../../ui-models/tab-item';

/**
 * Generated class for the FoodBankMenuListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-food-bank-menu-list',
  templateUrl: 'food-bank-menu-list.html',
})
export class FoodBankMenuListPage {
  items: TabItem[];

  constructor(
    public navCtrl: NavController,
    public navProxy: NavProxyProvider,
    public navParams: NavParams,
  ) {
    console.log({ navProxy });
    this.items = [
      {
        root: 'FoodBankDonationListPage',
        title: 'MENU_DONATION_LIST',
        icon: 'nutrition',
      },
      {
        root: 'FoodBankTagListPage',
        title: 'MENU_TAG_LIST',
        icon: 'pricetags',
      },
      {
        root: 'FoodBankReportRequestListPage',
        title: 'MENU_REPORT_REQUEST_LIST',
        icon: 'bookmarks',
      },
      {
        root: 'FoodBankReceiverListPage',
        title: 'MENU_RECEIVER_LIST',
        icon: 'restaurant',
      },
      {
        root: 'FoodBankDonorListPage',
        title: 'MENU_DONOR_LIST',
        icon: 'ribbon',
      },
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoodBankMenuListPage');
    this.onItemSelected(this.items[0]);
  }

  onItemSelected(item: TabItem) {
    this.navProxy.pushDetail(item.root, item);
  }
}

masterPages.push(FoodBankMenuListPage);
