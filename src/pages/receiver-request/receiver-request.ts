import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

enum tabs {
  request,
  food,
}

@IonicPage()
@Component({
  selector: 'page-receiver-request',
  templateUrl: 'receiver-request.html',
})
export class ReceiverRequestPage {
  tabs = tabs;
  tab: tabs = tabs.request;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReceiverRequestPage');
  }
}
