import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceiverFoodComponent } from '../../components/receiver-food/receiver-food';
import { ReceiverRequestComponent } from '../../components/receiver-request/receiver-request';
import { ReceiverRequestPage } from './receiver-request';

@NgModule({
  declarations: [
    ReceiverRequestPage,
    ReceiverRequestComponent,
    ReceiverFoodComponent,
  ],
  imports: [
    IonicPageModule.forChild(ReceiverRequestPage),
    TranslateModule.forChild(),
  ],
})
export class ReceiverRequestPageModule {}
