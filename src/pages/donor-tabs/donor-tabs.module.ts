import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DonorTabsPage } from './donor-tabs';

@NgModule({
  declarations: [DonorTabsPage],
  imports: [IonicPageModule.forChild(DonorTabsPage)],
})
export class DonorTabsPageModule {}
