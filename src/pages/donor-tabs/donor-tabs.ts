import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';
import { TabItem } from '../../ui-models/tab-item';

/**
 * Generated class for the DonorTabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-donor-tabs',
  templateUrl: 'donor-tabs.html',
})
export class DonorTabsPage {
  tabs: TabItem[];

  constructor(
    public navCtrl: NavController,
    public translateService: TranslateService,
  ) {
    this.tabs = [
      {
        root: 'DonorDonatePage',
        title: 'TAB_DONATION_TITLE',
        icon: 'nutrition',
      },
      {
        root: 'DonorReportPage',
        title: 'TAB_REPORT_TITLE',
        icon: 'bookmarks',
      },
      {
        root: 'DonorSettingPage',
        title: 'TAB_SETTING_TITLE',
        icon: 'cog',
      },
    ];
    const ts = this.tabs.map(x => x.title);
    translateService.get(ts).subscribe(values => {
      this.tabs.forEach(tab => (tab.title = values[tab.title]));
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DonorTabsPage');
  }
}
