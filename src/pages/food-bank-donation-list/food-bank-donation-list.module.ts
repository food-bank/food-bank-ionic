import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { FoodBankDonationListPage } from './food-bank-donation-list';

@NgModule({
  declarations: [FoodBankDonationListPage],
  imports: [
    IonicPageModule.forChild(FoodBankDonationListPage),
    TranslateModule.forChild(),
  ],
})
export class FoodBankDonationListPageModule {}
