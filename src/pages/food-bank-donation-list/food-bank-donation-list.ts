import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import swal from 'sweetalert2';
import { FoodBankDonationListItem } from '../../ui-models/food-bank-donation-list-item';

console.log(swal);

/**
 * Generated class for the FoodBankDonationListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-food-bank-donation-list',
  templateUrl: 'food-bank-donation-list.html',
})
export class FoodBankDonationListPage {
  items = FoodBankDonationListItem.samples;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoodBankDonationListPage');
  }

  send(item: FoodBankDonationListItem) {
    this.items = this.items.filter(x => x !== item);
    swal('Assigned', item.receiver_name + ' will be notified.', 'success');
  }
}
