import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoodBankTagListPage } from './food-bank-tag-list';

@NgModule({
  declarations: [FoodBankTagListPage],
  imports: [IonicPageModule.forChild(FoodBankTagListPage)],
})
export class FoodBankTagListPageModule {}
