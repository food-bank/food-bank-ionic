import { Component, OnDestroy, OnInit } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { showToast } from 'ioniclib/utils/toast';
import { Subscription } from 'rxjs/Subscription';
import { Donation } from '../../models/donation';
import { DonationItem } from '../../models/donation-item';
import { Tag } from '../../models/tag';
import { DonationProvider } from '../../providers/donation/donation';
import { TagProvider } from '../../providers/tag/tag';

/**
 * Generated class for the DonorDonationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'donor-donation',
  templateUrl: 'donor-donation.html',
})
export class DonorDonationComponent implements OnInit, OnDestroy {
  tags: Tag[] = [];
  items: DonationItem[];

  donation?: Donation;
  donor_id = 'Easy Farm';

  sub: Subscription;

  constructor(
    public tagService: TagProvider,
    public toastCtrl: ToastController,
    public donationService: DonationProvider,
  ) {
    console.log('Hello DonorDonationComponent Component');
    this.tagService.getTagList().then(xs => (this.tags = xs));
    this.items = [];
    this.addDonation();
  }

  ngOnInit(): void {
    this.sub = this.donationService
      .watchLastDonation(this.donor_id)
      .subscribe(donation => (this.donation = donation));
  }

  ngOnDestroy(): void {
    this.sub && this.sub.unsubscribe();
  }

  addDonation(): void {
    this.items.push({
      name: '',
      weight: 1,
      price: 0,
      tags: [],
    });
  }

  removeDonation(donation): void {
    const idx = this.items.indexOf(donation);
    this.items.splice(idx, 1);
  }

  donate(): void {
    console.log('I should be saving items list now... ', this.items);
    this.donationService
      .submitDonation(this.items, this.donor_id)
      .then(() =>
        showToast(this.toastCtrl, { message: 'Submitted, Thank you :)' }),
      )
      .catch(e => {
        console.error('failed to submit donation:', e);
        return showToast(this.toastCtrl, {
          message: 'Failed to submit, please try again later.',
        });
      });
  }

  get disabled(): boolean {
    return !!this.donation;
  }
}
