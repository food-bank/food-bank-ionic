import { NgModule } from '@angular/core';
import { DonorDonationComponent } from './donor-donation/donor-donation';
import { DonorReceiverComponent } from './donor-receiver/donor-receiver';
import { ReceiverFoodComponent } from './receiver-food/receiver-food';
import { ReceiverRequestComponent } from './receiver-request/receiver-request';
@NgModule({
  declarations: [
    DonorDonationComponent,
    DonorReceiverComponent,
    ReceiverRequestComponent,
    ReceiverFoodComponent,
  ],
  imports: [],
  exports: [
    DonorDonationComponent,
    DonorReceiverComponent,
    ReceiverRequestComponent,
    ReceiverFoodComponent,
  ],
})
export class ComponentsModule {}
