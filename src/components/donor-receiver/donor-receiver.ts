import { Component } from '@angular/core';

/**
 * Generated class for the DonorReceiverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'donor-receiver',
  templateUrl: 'donor-receiver.html',
})
export class DonorReceiverComponent {
  text: string;

  constructor() {
    console.log('Hello DonorReceiverComponent Component');
    this.text = 'ngo';
  }
}
