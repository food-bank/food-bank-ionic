import { Component } from '@angular/core';

/**
 * Generated class for the ReceiverRequestComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'receiver-request',
  templateUrl: 'receiver-request.html',
})
export class ReceiverRequestComponent {
  text: string;

  peopleNum: number;
  pickupTime: string;
  helperName: string;

  constructor() {
    console.log('Hello ReceiverRequestComponent Component');
  }

  requestResources() {
    const request = {
      people: this.peopleNum,
      time: this.pickupTime,
      helper: this.helperName,
    };
    console.log('requesting resources: ', request);
  }
}
