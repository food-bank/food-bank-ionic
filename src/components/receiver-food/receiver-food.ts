import { Component } from '@angular/core';

/**
 * Generated class for the ReceiverFoodComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'receiver-food',
  templateUrl: 'receiver-food.html',
})
export class ReceiverFoodComponent {
  donations: any[] = [];

  constructor() {
    console.log('Hello ReceiverFoodComponent Component');
    this.donations = [
      {
        donor: {
          name: 'test donor',
          location: {
            lat: 23.428,
            long: 120.74,
          },
        },
        resources: [
          {
            name: 'apple',
            weight: 12,
            tags: ['red', 'crispy'],
          },
          {
            name: 'wood',
            weight: 7,
            tags: ['teak', 'damaged'],
          },
        ],
      },
      {
        donor: {
          name: 'test donor2',
          location: {
            lat: 23.428,
            long: 120.74,
          },
        },
        resources: [
          {
            name: 'banana',
            weight: 4,
            tags: ['cavendish', 'large'],
          },
          {
            name: 'water',
            weight: 17,
            tags: ['spring', 'bottled'],
          },
        ],
      },
    ];
  }
}
