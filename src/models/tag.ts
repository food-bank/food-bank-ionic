export interface Tag {
  id: string;
  en: string;
  zh_tw: string;
}

export namespace Tag {
  export const tableName = 'tags';
}
