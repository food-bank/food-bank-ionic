export interface DonationItem {
  name: string;
  /* in kg */
  weight: number;
  /* in TWD */
  price: number;
  tags: string[];
}

export namespace DonationItem {
  export const candy: DonationItem = {
    name: 'Candy',
    weight: 2,
    price: 250,
    tags: ['Preserved Food'],
  };
  export const vegetable: DonationItem = {
    name: 'Tomato',
    weight: 5,
    price: 500,
    tags: ['Fresh Food'],
  };
}
