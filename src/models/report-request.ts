export interface ReportRequest {
  /* POSIX timestamp */
  start_date: number;
  end_date: number;
  create_time: number;
  donor_user_id: string;
  completed: boolean;
}
export namespace ReportRequest {
  export let tableName = 'report_requests';
}
