import { DonationItem } from './donation-item';
import { User } from './user';

export interface Donation {
  id: string;
  create_time: number;
  items: DonationItem[];
  donor_id: string;
  receiver_id?: string;
  /* name, free text */
  receiving_helper?: string;
}

export namespace Donation {
  export const tableName = 'donations';
  export const tomato = {
    id: 'd-1',
    create_time: Date.now(),
    items: [DonationItem.candy],
    donor_id: User.shop.id,
    receiver_id: undefined,
    receiving_helper: undefined,
  };
  export const candy: Donation = {
    id: 'd-2',
    create_time: Date.now(),
    items: [DonationItem.vegetable],
    donor_id: User.farm.id,
    receiver_id: undefined,
    receiving_helper: undefined,
  };
  export const samples: Donation[] = [tomato, candy];
}
