import { sha256 } from 'typestub-sha256';

export interface User {
  id: string;
  groups: string[];
  email: string;
  password_hash: string;
  name: string;
  donor: boolean;
  receiver: boolean;
  food_bank: boolean;
  latlng?: { lat: number; lng: number };
}

export namespace User {
  export const tableName = 'users';
  export const bank = {
    id: 'bank-1',
    groups: undefined,
    email: 'bank@example.com',
    password_hash: sha256('password'),
    name: 'Taiwan Food Bank',
    donor: undefined,
    receiver: undefined,
    food_bank: true,
  };
  export const farm = {
    id: 'farm-1',
    groups: undefined,
    email: 'farm@example.com',
    password_hash: sha256('password'),
    name: 'Easy Farm',
    donor: true,
    receiver: undefined,
    food_bank: undefined,
  };
  export const shop = {
    id: 'shop-1',
    groups: undefined,
    email: 'shop@example.com',
    password_hash: sha256('password'),
    name: 'Happy Shop',
    donor: true,
    receiver: undefined,
    food_bank: undefined,
  };
  export const kid = {
    id: 'ngo-1',
    groups: undefined,
    email: 'kid@example.com',
    password_hash: sha256('password'),
    name: 'Sunny Kids',
    donor: undefined,
    receiver: true,
    food_bank: undefined,
  };
  export const mama = {
    id: 'ngo-2',
    groups: undefined,
    email: 'mama@example.com',
    password_hash: sha256('password'),
    name: 'Easy Farm',
    donor: undefined,
    receiver: true,
    food_bank: undefined,
  };
  export const samples: User[] = [bank, farm, shop, kid, mama];
}
